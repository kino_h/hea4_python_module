__verson__ = "0.2.0"

import seaborn as sns
import pandas as pd
from pymatgen.core.periodic_table import Element
import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
import plotly.graph_objects as go


class PeriodicTableBasic(object):
    """basic class of PeriodicTable
    """

    def __init__(self):
        """initialization of periodic table class
        """
        symbol_list = []
        z_list = []
        row_list = []
        group_list = []
        for z in range(1, 104):
            elm = Element('H').from_Z(z)
            symbol_list.append(str(elm))
            z_list.append(z)
            row_list.append(elm.row)
            group_list.append(elm.group)
        df_ptable = pd.DataFrame(
            {"Z": z_list, "symbol": symbol_list, "row": row_list, "group": group_list})
        self.df_ptable = df_ptable

        row_max = np.array(row_list).max()
        group_max = np.array(group_list).max()
        row_labels = []
        for row in range(row_max):
            row_labels.append(str(row+1))
        group_labels = []
        for group in range(group_max):
            group_labels.append(str(group+1))

        self.row_labels = row_labels
        self.group_labels = group_labels

        prop_ptable = np.empty((row_max, group_max))
        prop_ptable[:, :] = np.nan
        self.prop_ptable = prop_ptable
        
        symbol_ptable = np.empty((row_max, group_max), dtype=object)
        symbol_ptable[:, :] = ""
        self.symbol_ptable = symbol_ptable

class PeriodicTableProp(PeriodicTableBasic):
    """prioricTable property class
    """

    def __init__(self, z_list: (np.array, list), prop_list: (np.array, list), newline="\n"):
        """initialization

        Args:
            z_list (np.array, list): a list of atomic number
            prop_list (np.array, list): a list of property
        """
        super().__init__()
        df_select = pd.DataFrame({"Z": z_list, "_target_values_": prop_list})
        df_ptable_select = self.df_ptable.merge(df_select, on="Z")

        annot_ptable = np.empty_like(self.prop_ptable, dtype=object)

        prop_ptable = self.prop_ptable
        symbol_ptable = self.symbol_ptable
        columns = ["Z", "symbol", "row", "group", "_target_values_"]
        for Z, symbol, row, group, R in df_ptable_select[columns].values:
            row, group = int(row), int(group)
            prop_ptable[row-1, group-1] = R
            annot_ptable[row-1, group-1] = "{}{}{:.1f}".format(symbol, newline, R)
            symbol_ptable[row-1, group-1] = symbol
        self.annot_ptable = annot_ptable

        self.df_prop_ptable = pd.DataFrame(
            prop_ptable, index=self.row_labels, columns=self.group_labels)
        
        self.symbol_ptable = symbol_ptable

    def show_heatmap(self, filename=None, figsize=(15, 6)):
        """show heatmap of data

        Args:
            filename (str, optional): filename to save. Defaults to None.
            figsize (tuple, optional): figure size. Defaults to (15, 6).
        """
        plt.figure(figsize=figsize, facecolor="w")
        df_prop_ptable = self.df_prop_ptable
        annot_ptable = self.annot_ptable
        sns.heatmap(df_prop_ptable, annot=annot_ptable, fmt="s",
                    linewidths=1, linecolor="lightgray", cmap="coolwarm")
        plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False,
            labeltop=True)
        plt.tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=True)

        plt.tight_layout()
        if filename is not None:
            plt.savefig(filename)
        plt.show()

    def show_go_bubble_plot(self):

        def make_df_p(df, df_symbol):
            dx = 0.0
            x_list = []
            for i in range(df.shape[0]):
                for j in range(df.shape[1]):
                    r = df.iloc[i,j]
                    symbol = df_symbol.iloc[i,j]
                    if not np.isnan(r):
                        x_list.append([i+dx,j+dx,r, symbol])

            df_p = pd.DataFrame(x_list, columns=["row","group","size","symbol"])
            return df_p

        def make_rmax(dfp):
            rvalues = dfp.loc[:,"size"].values.reshape(-1)
            i = ~np.isnan(rvalues)
            rvalues0 = dfp.loc[i,"size"].values.reshape(-1)

            rmax = rvalues0.max()
            return rmax


        def make_go_plot(df, width=600):
            df_ = df.T
            # Set figure size
            height=float(width)*(2+df_.shape[1])/(df_.shape[0])

            fig = px.scatter(df_p, x="group", y="row", size="size", color="size", hover_data=["symbol"])
            #fig = go.Figure(data=go.Scatter(x=df_p["group"], y=df_p["row"], size=df_p["size"], mode="markers", 
            #                                marker_color=df_p["size"], text=["symbol"]))

            dx = 0.5
            xlim = (-dx, df_.shape[0]-dx)
            ylim = (df_.shape[1]-dx, -dx)
            if False:
                fig.update_xaxes(showgrid=False)
                fig.update_yaxes(showgrid=False)

            fig.update_xaxes(showgrid=True, tickvals=np.arange(df_.shape[0]),
                             ticktext=list(map(str, np.arange(1,df_.shape[0]+1))),
                             zerolinewidth=3, gridwidth=3,
                             side="top", range=xlim)
            fig.update_yaxes(showgrid=True, tickvals=np.arange(df_.shape[1]),
                             ticktext=list(map(str, np.arange(1,df_.shape[1]+1))),
                             zerolinewidth=3, gridwidth=3,
                             range=ylim)   

            fig.update_layout(width = width, height=height)


            fig.show()

        df = pd.DataFrame(self.prop_ptable)
        df_annot = pd.DataFrame(self.annot_ptable)
        df_symbol = pd.DataFrame(self.symbol_ptable)

        df_p = make_df_p(df, df_symbol)
        rmax = make_rmax(df_p)
        make_go_plot(df)


# In[2]:


if __name__ == "__main__":
    def make_atomic_property():
        z_list = []
        prop_list = []
        for z in range(1,104):
            elm = Element('H').from_Z(z)
            prop_str = str(elm.atomic_radius)
            if prop_str is not None:
                prop_str = prop_str.replace(" ang","")
                if prop_str == "None":
                    prop = None
                else:
                    prop = float(prop_str)
            else:
                prop = None
            if prop is not None:
                z_list.append(z)
                prop_list.append(prop)
        return z_list, prop_list

    def main():
        z_list, prop_list = make_atomic_property()

        pt = PeriodicTableProp(z_list, prop_list)
        pt.show_go_bubble_plot()
        
    main()


# In[ ]:




