#!/usr/bin/env python
# coding: utf-8

from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash
import numpy as np
from pymatgen.core.periodic_table import Element
import dash_html_components as html
import copy


class PeriodicTable:
    """Periodic Table supplementry class

    zmax : max number of z +1
    symbol_list : atomic element names from H,...
    row_list: atomic element row from H,...
    group_list: atomic element group from H,...
    row_max: max. number of row_list
    group_max: max. number of group_list
    """

    def __init__(self):
        """initialization
        """
        zmax = 104
        row_list = []
        group_list = []
        z2symbol = []
        for i in range(1, zmax):
            elm = Element('H').from_Z(i)
            z2symbol.append(str(elm))
            row_list.append(elm.row)
            group_list.append(elm.group)

        self.zmax = zmax
        self.symbol_list = z2symbol
        self.row_list = row_list
        self.group_list = group_list
        self.row_max = np.max(row_list)
        self.group_max = np.max(group_list)


class DashPtable:
    """selectable peridic table for dash
    """

    def __init__(self):
        """initialization
        """
        self.make_table_content()
        self.make_attribute()
        self.make_html_button_list()

    def make_table_content(self):
        """make table_content

        table_content: np.array
        """
        pt = PeriodicTable()
        self.periodic_table = pt

        initial_length = 3
        table_content = []
        row_content = []
        row_max = pt.row_max
        group_max = pt.group_max
        for row in range(1, row_max + 1):
            row_content = []
            for group in range(1, group_max + 1):
                row_content.append(' ' * initial_length)
            table_content.append(row_content)
        table_content = np.array(table_content)
        for elm, row, group in zip(pt.symbol_list, pt.row_list, pt.group_list):
            table_content[row - 1, group - 1] = elm

        table_content[table_content == ' ' * initial_length] = ''
        self.table_content = table_content

    def make_attribute(self):
        """make button attributes
        """
        default_button = {'width': '2.2em', "text-align": "center"}
        exposed_button = copy.deepcopy(default_button)
        exposed_button.update({'background-color': 'gray'})
        normal_button = copy.deepcopy(default_button)
        normal_button.update({'background-color': 'white'})
        clicked_button = copy.deepcopy(default_button)
        clicked_button.update({'background-color': 'red'})
        self.default_button = default_button
        self.exposed_button = exposed_button
        self.normal_button = normal_button
        self.clicked_button = clicked_button

    def make_html_button_list(self, button_prefix='ptable_button_'):
        """make html button list

        html_buton_list (list): a list of html buttons
        table_content_list (list): a list of table content (should be the same as periodic_table.symbol_list)
        table_content_id_list (list): a list of table content id

        Args:
            button_prefix (str, optional): prefix to add button names. Defaults to 'ptable_button_'.
        """
        html_button_list = []
        table_content_list = []
        table_content_id_list = []
        table_content = self.table_content
        exposed_button = self.exposed_button
        n0 = table_content.shape[0]
        n1 = table_content.shape[1]
        for i in range(n0):
            b = []
            for j in range(n1):
                button_id = button_prefix + table_content[i, j]
                if len(table_content[i, j]) > 0:
                    b.append(html.Button(children=table_content[i, j],
                                         id=button_id,
                                         style=exposed_button))
                    table_content_list.append(table_content[i,j])
                    table_content_id_list.append(button_id)
                else:
                    b.append(None)

            html_button_list.append(b)
        self.html_button_list = html_button_list
        self.table_content_list = table_content_list
        self.table_content_id_list = table_content_id_list

    def make_html_table(self):
        """make html.Table

        Returns:
            html.Table: html table of periodic table
        """
        html_button_list = self.html_button_list
        n0 = len(html_button_list)
        n1 = len(html_button_list[0])
        html_table = html.Table([
            html.Tbody([
                html.Tr([
                        html.Td(html_button_list[i][j]) for j in range(n1)
                        ]) for i in range(n0)
            ])
        ])
        return html_table

    def make_state_list(self):
        """make input list for @app.callback

        Returns:
            list: a list of Input
        """
        input_list = []
        for name in self.table_content_id_list:
            input_list.append(State(name, 'n_clicks'))
        return input_list

    def convert_to_selected(self, *args):
        selected_list = []
        for name, n_clicks in zip(self.table_content_list, args):
            if n_clicks is not None:
                if  n_clicks % 2 == 1:
                    print("add",name)
                    selected_list.append(name)
        return selected_list

if __name__ == '__main__':
    # global variable
    dash_ptable = DashPtable()

    for x in dash_ptable.table_content_id_list:
        print("Input('{}', 'n_clicks'),".format(x))

    app = dash.Dash(__name__)

    app.layout = html.Div(children=[
        html.H2(children='periodic table'),
        html.Div(children='', id='outtext'),
        dcc.RadioItems(id='selection-type',
                       options=[{'label': i, 'value': i}
                                for i in ['AND', 'OR']],
                       value='AND',
                       labelStyle={'display': 'inline-block'}),
        dash_ptable.make_html_table(),
        html.Div(children=[
                 html.Button(children='start-plot', id='start-plot')],
                 style={'display': 'flex', 'justify-content': 'center'})
    ])

    for button_id in reversed(dash_ptable.table_content_id_list):
        @app.callback(Output(button_id, 'style'),
                      [Input(button_id, 'n_clicks')]
                      )
        def callback_onclick(n_clicks):
            if n_clicks is None:
                return dash_ptable.normal_button
            if n_clicks % 2 == 0:
                return dash_ptable.normal_button
            else:
                return dash_ptable.clicked_button

    app.run_server(debug=False)
