import copy
import numpy as np
from pymatgen.core.periodic_table import Element

import dash
import dash_core_components as dcc

import dash_html_components as html
from dash.dependencies import Input, Output


class PeriodicTable_table:
    """A class to make selectable periodic table using plotly-dash.
    """

    def __init__(self):
        """initialization
        """
        self.table_content, self.button_content = self.make_tables()
        self.exposed_button, self.normal_button,\
            self.clicked_button = self.make_button_attributes()
        self.button_id_list, self.button_name_list,\
            self.button_id_dic, self.html_button =\
            self.make_html_button(self.table_content,
                                  self.button_content,
                                  self.exposed_button)
        self.html_table = self.make_html_table(self.html_button)

    def make_tables(self):
        """make periodic table and their button names

        Returns:
            np.array: symbols of the periodic table
            np.array: button names of the periodic table for internal use
        """
        zmax = 104
        row_list = []
        group_list = []
        for i in range(1, zmax):
            elm = Element('H').from_Z(i)
            row_list.append(elm.row)
            group_list.append(elm.group)
        row_max = np.max(row_list)
        group_max = np.max(group_list)

        initial_length = 3
        table_content = []
        row_content = []
        for row in range(1, row_max + 1):
            row_content = []
            for group in range(1, group_max + 1):
                row_content.append(' ' * initial_length)
            table_content.append(row_content)
        table_content = np.array(table_content)
        for i in range(1, zmax):
            elm = Element('H').from_Z(i)
            table_content[elm.row - 1, elm.group - 1] = str(elm)

        table_content[table_content == ' ' * initial_length] = ''

        button_content = []
        for row in range(1, row_max + 1):
            row_content = []
            for group in range(1, group_max + 1):
                row_content.append('group_{}_{}'.format(row, group))
            button_content.append(row_content)
        button_content = np.array(button_content)
        return table_content, button_content

    def make_button_attributes(self):
        """make unselected and selected button html attributes

        Returns:
            dic: exposed button style
            dic: normal button style
            dic: selected button style
        """
        default_button = {'width': '30px', "text-align": "center"}
        exposed_button = copy.deepcopy(default_button)
        exposed_button.update({'background-color': 'gray'})
        normal_button = copy.deepcopy(default_button)
        normal_button.update({'background-color': 'white'})
        clicked_button = copy.deepcopy(default_button)
        clicked_button.update({'background-color': 'red'})
        # print(normal_button)
        # print(clicked_button)
        return exposed_button, normal_button, clicked_button

    def make_html_button(self, table_content, button_content, exposed_button):
        """[summary]

        Args:
            table_content (np.array): symbols of the periodilc table
            button_content ([np.array): button names of the periodic table for internal use
            exposed_button (dict): unselected button attributes

        Returns: 
            list: a list of button ids
            list: a list of button names
            dict: dict of button names, all values are False
            list: a list of html button object list
        """
        button_id_list = []
        button_id_dic = {}
        button_name_list = []

        html_button_list = []
        n0 = table_content.shape[0]
        n1 = table_content.shape[1]
        for i in range(n0):
            b = []
            for j in range(n1):
                s = button_content[i, j]
                button_id_list.append(s)
                button_name_list.append(table_content[i, j])
                if len(table_content[i, j]) > 0:
                    b.append(html.Button(children=table_content[i, j],
                                         id=s,
                                         style=exposed_button))
                else:
                    b.append(None)

            html_button_list.append(b)
        # print(button_id_list)

        for s in button_id_list:
            button_id_dic[s] = False

        return button_id_list, button_name_list, button_id_dic, html_button_list

    def make_html_table(self, html_button_list):
        """make html.Table periodic table

        Args:
            html_button_list ([type]): [description]

        Returns:
            html.Table: html.Table periodic table
        """
        n0 = len(html_button_list)
        n1 = len(html_button_list[0])
        html_table = html.Table([
            html.Tbody([
                html.Tr([
                    html.Td(html_button_list[i][j]) for j in range(n1)
                ]) for i in range(n0)
            ])
        ])

        return html_table

    def _get_selected(self):
        """make a list of selected elements

        Returns:
            list: a list of selected elements
        """
        selected_elements = []
        for d in self.button_id_dic.keys():
            if self.button_id_dic[d]:
                i, j = np.where(self.button_content == d)
                i = i[0]
                j = j[0]
                elm = self.table_content[i, j]
                selected_elements.append(elm)
        return selected_elements

    @property
    def selected(self):
        """make a list of selected elements

        Returns:
            list: a list of selected elements
        """
        return self._get_selected()

    if False:
        def callback_onclick(self,button):
            if button is None:
                # this is one of the first times of the current session
                # initialize self.selected
                for s in self.button_id_list:
                    self.button_id_dic[s] = False

                print("button is none selected=",self.selected)
                return self.normal_button

            ctx = dash.callback_context
            if not ctx.triggered or ctx.triggered[0]['value'] is None:
                return self.exposed_button
            else:
                clicked_id = ctx.triggered[0]['prop_id'].split('.')[0]
                clicked_value = int(ctx.triggered[0]['value'])
                if clicked_value % 2 == 0:
                    self.button_id_dic[clicked_id] = False
                    print(self.selected)
                    return self.normal_button
                else:
                    self.button_id_dic[clicked_id] = True
                    print(self.selected)
                    return self.clicked_button


if __name__ == '__main__':
    # global variable
    ptable = PeriodicTable_table()

    app = dash.Dash(__name__)

    app.layout = html.Div(children=[
        html.H2(children='periodic table'),
        html.Div(children='', id='outtext'),
        dcc.RadioItems(id='selection-type',
                        options=[{'label': i, 'value': i} for i in ['AND', 'OR']],
                        value='AND',
                        labelStyle={'display': 'inline-block'}), 
        ptable.html_table,
        html.Div(children=[
                 html.Button(children='start-plot', id='start-plot')],
                 style={'display': 'flex', 'justify-content': 'center'})
    ])

    # for name, s in zip(ptable.button_name_list, ptable.button_id_list):
    for name, s in zip(reversed(ptable.button_name_list),
                       reversed(ptable.button_id_list)):
        # print("callback", name, s)
        if len(name) > 0:
            @app.callback(
                Output(s, 'style'),
                [Input(s, 'n_clicks')])
            def onclick(button):
                return ptable.callback_onclick(button)

    # global variable
    last_start_plot = None

    @app.callback(
        Output('outtext', 'children'),
        [Input('selection-type', 'value'),
         Input('start-plot', 'n_clicks')])
    def do_plot(selection_type, start_plot):
        global last_start_plot
        print(selection_type, start_plot, ptable.selected)
        if start_plot is not None and len(ptable.selected) > 0:
            doit = False
            if last_start_plot is None:
                doit = True
            else:
               if last_start_plot != start_plot:
                   doit = True 
            if doit:
                print("doit",selection_type, start_plot, ptable.selected)
        if start_plot is not None:
            last_start_plot = start_plot
        return None

    app.run_server(debug=False)
