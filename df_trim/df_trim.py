#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import random

class DfTrim:
    """Trim DataFrame
    """

    def __init__(self, df, key, group, ndiv=10, binmax=1000, eps=1e-3):
        """initialization

        Args:
            df (DataFrame): data
            key (list): a list of key to divide data space
            group (list): a list of group name to divide data space (for internal use)
            ndiv (int, optional): number of divisions for each axis. Defaults to 10.
            binmax (int, optional): the max. number inside each bin. Defaults to 1000.
            eps (float, optional): eps to define range. Defaults to 1e-3.
        """
        key_list = key
        key_group_list = group

        for key, key_group in zip(key_list, key_group_list):
            df = self.add_key_group(
                df, key, group=key_group, ndiv=ndiv, eps=eps)

        df_groups, _ = self.make_df_groups(df, key_group_list)
        df_groups_trim = self.make_groups_trim(df_groups, binmax)
        self.df_trim = pd.concat(df_groups_trim, axis=0)

        # debug
        self.df_groups = df_groups
        self.df_groups_trim = df_groups_trim

    @ property
    def df(self) -> pd.DataFrame:
        """returns trimmed data

        Returns:
            pd.DataFrame: trimed data
        """
        return self.df_trim

    def add_key_group(self, df, key, group, ndiv, eps):
        """make groups to use groupby()

        Args:
            df (DataFrame): data
            key (list): a list of data keys to make bins
            group (list): a list of group names to make bins to use groupby()
            ndiv (int): the number of division for each axis
            eps (float): small number to add to make bins

        Returns:
            [type]: [description]
        """
        if group is None:
            group = copy.deepcopy(key).replace(" ", "_") + "_group"
        print("add", group)
        if key in list(df.columns) and group not in list(df.columns):
            val = df[key].astype(float).values
            sval = (val-val.min())/(val.max()+eps - val.min()) * ndiv
            ival = list(map(int, sval))
            df[group] = ival
        return df

    def make_df_groups(self, df: pd.DataFrame, key_group_list: list):
        """[summary]

        Args:
            df (pd.DataFrame): dataframe
            key_group_list (list): group list to execute groupby()

        Returns:
            DataFrame: a list of grouped dataframe
            list: a list of group index
        """

        iall = 0
        group_names = []
        df_groups = []
        for _name, _df in df.groupby(key_group_list):
            iall += _df.shape[0]
            df_groups.append(_df)
            group_names.append(_name)
        return df_groups, group_names

    def make_groups_trim(self, df_groups: list, binmax: int):
        """[summary]

        Args:
            df_groups (list): a list of grouped dataframe
            binmax (int): the max. number of datapoints inside each bins

        Returns:
            pd.DataFrame: trimmed dataframe
        """
        df_groups_trim = []
        # iall = 0
        for _df in df_groups:
            _df.reset_index(drop=True, inplace=True)
            nmax = _df.shape[0]
            if nmax > binmax:
                all_list = list(_df.index)
                selected_list = random.sample(all_list, binmax)
                _df_trim = _df.iloc[selected_list, :]

                # print("selected", nmax, "->", _df_trim.shape[0])
                # iall += _df_trim.shape[0]
                df_groups_trim.append(_df_trim)
            else:
                # iall += _df.shape[0]
                df_groups_trim.append(_df)

        # print("reduced total number of samples", iall)
        return df_groups_trim
